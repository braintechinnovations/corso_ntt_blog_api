package com.lezione28.blog.api.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.tomcat.util.threads.ResizableExecutor;

import com.lezione28.blog.api.connessioni.ConnettoreDB;
import com.lezione28.blog.api.model.Articolo;

public class ArticoloDAO implements Dao<Articolo>{

	@Override
	public Articolo getById(int id) throws SQLException {
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT articoloid, titolo, slug, descrizione FROM articolo WHERE articoloid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Articolo temp = new Articolo();
   		temp.setId(risultato.getInt(1));
   		temp.setTitolo(risultato.getString(2));
   		temp.setSlug(risultato.getString(3));
   		temp.setDescrizione(risultato.getString(4));
       	
       	return temp;
		
	}

	@Override
	public ArrayList<Articolo> getAll() throws SQLException {

		ArrayList<Articolo> elenco = new ArrayList<Articolo>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT articoloid, titolo, slug, descrizione FROM articolo";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Articolo temp = new Articolo();
       		temp.setId(risultato.getInt(1));
       		temp.setTitolo(risultato.getString(2));
       		temp.setSlug(risultato.getString(3));
       		temp.setDescrizione(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Articolo t) throws SQLException {
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO articolo (titolo, descrizione) VALUES (?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getTitolo());
       	ps.setString(2, t.getDescrizione());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	@Override
	public boolean delete(Articolo t) throws SQLException {
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM articolo WHERE articoloid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Articolo t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE articolo SET "
   				+ "titolo = ?, "
   				+ "descrizione = ? "
   				+ "WHERE articoloid = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getTitolo());
       	ps.setString(2, t.getDescrizione());
       	ps.setInt(3, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	

}
