package com.lezione28.blog.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione28.blog.api.model.Articolo;
import com.lezione28.blog.api.services.ArticoloDAO;
import com.lezione28.blog.api.utility.ResponsoOperazione;

@WebServlet("/recuperaarticoli")
public class RecuperaArticoli extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			ArticoloDAO artDao = new ArticoloDAO();
			
			Integer identificatore = request.getParameter("id_articolo") != null ? Integer.parseInt(request.getParameter("id_articolo")) : -1;
			
			try {
				
				if(identificatore == -1) {
					ArrayList<Articolo> elencoArticoli = artDao.getAll();
					
					String risultatoJson = new Gson().toJson(elencoArticoli);
					Thread.sleep(500);
					out.print(risultatoJson);
				}
				else {
					Articolo temp = artDao.getById(identificatore);
					out.print(new Gson().toJson(temp));
				}

			} catch (SQLException | InterruptedException e) {

				out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
				System.out.println(e.getMessage());
				
			}
	}

}
