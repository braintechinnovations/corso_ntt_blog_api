package com.lezione28.blog.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione28.blog.api.model.Articolo;
import com.lezione28.blog.api.services.ArticoloDAO;
import com.lezione28.blog.api.utility.ResponsoOperazione;


@WebServlet("/rimuoviarticolo")
public class RimuoviArticolo extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");		//In modo da non dover invocare il JSON.parse dal AJAX
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();
		
		Integer identificatore = request.getParameter("id_articolo") != null ? Integer.parseInt(request.getParameter("id_articolo")) : -1;
		
		if(identificatore != -1) {

			ArticoloDAO art_dao = new ArticoloDAO();
			try {
				
				Articolo art = art_dao.getById(identificatore);
				if(art_dao.delete(art)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(jsonizzatore.toJson(res));
				}
				else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
					out.print(jsonizzatore.toJson(res));
				}
				
			} catch (SQLException e) {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
				out.print(jsonizzatore.toJson(res));
			}
			
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
			out.print(jsonizzatore.toJson(res));
		}
	}

}
