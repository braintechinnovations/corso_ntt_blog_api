package com.lezione28.blog.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

class Responso{
	public String risultato;
}

@WebServlet("/provegetpost")
public class provegetpost extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String par_uno = request.getParameter("primo");
		String par_due = request.getParameter("secondo");
		
		PrintWriter out = response.getWriter();
//
//		out.println("SONO LA GET");
//		out.println(par_uno);
//		out.println(par_due);	
//		
		Responso res = new Responso();
		res.risultato = "OK";

		out.println(new Gson().toJson(res));	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String par_uno = request.getParameter("primo");
		String par_due = request.getParameter("secondo");
		
		PrintWriter out = response.getWriter();

		out.println("SONO LA POST");
		out.println(par_uno);
		out.println(par_due);
	}

}
