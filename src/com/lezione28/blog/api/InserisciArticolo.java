package com.lezione28.blog.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione28.blog.api.model.Articolo;
import com.lezione28.blog.api.services.ArticoloDAO;
import com.lezione28.blog.api.utility.ResponsoOperazione;

//class ResponsoOperazione{
//	private String risultato;
//	private String dettaglio;
//
//	public ResponsoOperazione(String risultato, String dettaglio) {
//		super();
//		this.risultato = risultato;
//		this.dettaglio = dettaglio;
//	}
//}

@WebServlet("/inserisciarticolo")
public class InserisciArticolo extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String varTitolo = request.getParameter("input_titolo");
		String varDescrizione = request.getParameter("input_descrizione");
		
		Articolo temp = new Articolo();
		temp.setTitolo(varTitolo);
		temp.setDescrizione(varDescrizione);
		
		ArticoloDAO art_dao = new ArticoloDAO();
		String strErrore = "";
		try {
			art_dao.insert(temp);
		} catch (SQLException e) {
			strErrore = e.getMessage();
		}
		
		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();
		
		if(temp.getId() != null) {
//			out.print(jsonizzatore.toJson(new ResponsoOperazione("OK")));
			
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
		}
	}

}

/*
 * 1. Ristrutturate la visualizzazione degli articoli in modo che mi venga visualizzata
 * una tabella (e non le card) nella index.html.
 * 
 * 2. Sempre nella index.html mi permettete l'inserimento di nuovo articolo tramite MODALE
 * 
 * 3. Di fianco ad ogni riga, mettere un pulsante per l'eleminazione dell'articolo.
 * 
 * 4. HARD: Di fianco ad ogni pulsante di eliminazione creare un pulsante di modifica che 
 * fa comparire un'altra modale per la modifica dell'articolo.
 */
