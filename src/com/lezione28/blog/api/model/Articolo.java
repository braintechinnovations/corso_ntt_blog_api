package com.lezione28.blog.api.model;

public class Articolo {

	private Integer id;
	private String titolo;
	private String slug;
	private String descrizione;
	
	
	public Articolo() {
		
	}

	public Articolo(Integer id, String titolo, String slug, String descrizione) {
		this.id = id;
		this.titolo = titolo;
		this.slug = slug;
		this.descrizione = descrizione;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Articolo [id=" + id + ", titolo=" + titolo + ", slug=" + slug + ", descrizione=" + descrizione + "]";
	}
	
	
	
	
}
