package com.lezione28.blog.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione28.blog.api.model.Articolo;
import com.lezione28.blog.api.services.ArticoloDAO;
import com.lezione28.blog.api.utility.ResponsoOperazione;

@WebServlet("/aggiornaarticolo")
public class AggiornaArticolo extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer idArticolo = request.getParameter("id_articolo") != null ? Integer.parseInt(request.getParameter("id_articolo")) : -1;
		String titoloArticolo = request.getParameter("titolo") != null ? request.getParameter("titolo") : "";
		String descrizioneArticolo = request.getParameter("descrizione") != null ? request.getParameter("descrizione") : "";
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ArticoloDAO artDao = new ArticoloDAO();
		
		if(idArticolo == -1 || titoloArticolo.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "ID o TITOLO non inseriti!")));
			return;
		}
		
		try {
			Articolo temp = artDao.getById(idArticolo);
			
			temp.setTitolo(titoloArticolo);
			temp.setDescrizione(descrizioneArticolo);
			
			if(artDao.update(temp)) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Articolo non trovato!")));
		}
	}

}
