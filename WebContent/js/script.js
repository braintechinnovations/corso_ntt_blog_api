function stampaCards(arr_articoli){
	let contenuto = "";
	
	for(let i=0; i<arr_articoli.length; i++){
		contenuto += cardRisultato(arr_articoli[i]);
	}
	
	$("#contenuto-richiesta").html(contenuto);
}

function cardRisultato(obj_articolo){
	
	let risultato = '<tr data-identificatore="' + obj_articolo.id + '">';
	risultato += '    <td>' + obj_articolo.titolo + '</td>';
	risultato += '    <td>' + obj_articolo.descrizione.substr(0, 60) + '...</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaArticolo(this)">E</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaArticolo(this)">M</button></td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function modificaArticolo(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTAPI/recuperaarticoli",
				method: "POST",
				data: {
					id_articolo: idSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					
					$("#titolo_edit").val(responso.titolo);
					$("#descrizione_edit").val(responso.descrizione);
					$("#modaleModifica").data("identificatore", idSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

function eliminaArticolo(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTAPI/rimuoviarticolo",
				method: "POST",
				data: {
					id_articolo: idSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
					}
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
	
	
	
//	let idSelezionato = $(objButton).parent().parent().data("identificatore");
//	console.log(idSelezionato);
}

function aggiornaTabella(){
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTAPI/recuperaarticoli",
				method: "POST",
				success: function(ris_success){
//					console.log("TUTTO OK");
					
					//let risJson = JSON.parse(ris_success);	//Superfluo con encoding
					stampaCards(ris_success);
//					console.log(risJson);
				},
				error: function(ris_error){
					console.log("ERRORE");
					console.log(ris_error);
				}
			}
	);
}


$(document).ready(
		function(){

			aggiornaTabella();			//richiamo l'aggiornamento della tabella al caricamento della pagina
			
			window.setInterval(
					function(){
						aggiornaTabella();
						console.log("AGGIORNATO");
					}
			, 3000);
			
//			$("#cta-console").click(
//					function(){
//						console.log("CIAO GIOVANNI");
//					}
//			);
			
			$("#cta-leggi").click(
					function(){
						aggiornaTabella();
					}
			);
			
			$("#cta-aggiungi").click(
					function(){
						$("#modaleInserimento").modal("show");
					}
			);
			
			
			$("#cta-inserisci").click(
					function(){
						let varTitolo = $("#titolo").val();
						let varDescrizione = $("#descrizione").val();

						$.ajax(
								{
									url: "http://localhost:8080/CorsoNTTAPI/inserisciarticolo",
									method: "POST",
									data: {
										input_titolo: varTitolo,
										input_descrizione: varDescrizione
									},
									success: function(risultato){
										console.log("SONO IN SUCCESS");
										let risJson = JSON.parse(risultato);
										
										switch(risJson.risultato){
										case "OK":
											aggiornaTabella();
											$("#modaleInserimento").modal("toggle");
											alert("Inserito con successo");
											break;
										case "ERRORE":
											alert("ERRORE DI INSERIMENTO :(\n" + risJson.dettaglio);
											break;
										}
										
										console.log(risJson);
									},
									error: function(errore){
										console.log("SONO IN ERROR");
										let errJson = JSON.parse(errore);
										console.log(errJson);
									}
									
								}
						);
						
					}
			);
			

			$("#cta-modifica").click(
					function(){
						let idSelezionato = $("#modaleModifica").data("identificatore");

						$.ajax(
								{
									url: "http://localhost:8080/CorsoNTTAPI/aggiornaarticolo",
									method: "POST",
									data: {
										id_articolo: idSelezionato,
										titolo: $("#titolo_edit").val(),
										descrizione: $("#descrizione_edit").val()
									},
									success: function(responso){
										switch(responso.risultato){
										case "OK":
											$("#modaleModifica").modal("toggle");
											aggiornaTabella();
											break;
										case "ERRORE":
											alert(responso.risultato + "\n" + responso.dettaglio);
											break;
										}
									},
									error: function(errore){
										console.log(errore);
									}
								}
						);
						
					}
			);
			
			
		}
);