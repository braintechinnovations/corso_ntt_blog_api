<%@page import="com.lezione28.blog.api.model.Articolo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lezione28.blog.api.services.ArticoloDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

	<div class="container">
		<div class="row">
			<div class="col-md-2">
				
				<div class="row mt-2">
					<div class="col">
						<button type="button" class="btn btn-primary btn-block" id="cta-leggi">Leggi i libri</button>
					</div>
				</div>
				<div class="row mt-2">
					<div class="col">
						<button type="button" class="btn btn-success btn-block" id="cta-aggiungi">Aggiungi libro</button>
					</div>
				</div>
				
			</div>
			<div class="col-md-10">
				
				<table class="table">
					<thead>
						<tr>
							<th>Titolo</th>
							<th>Descrizione</th>
							<th>Azioni</th>
						</tr>
					</thead>
					<tbody id="contenuto-richiesta">
					
						<%
							ArticoloDAO artDao = new ArticoloDAO();
							ArrayList<Articolo> elenco = artDao.getAll();
							
							String risultato = "";
							for(int i=0; i<elenco.size(); i++){
								Articolo temp = elenco.get(i);

								risultato += "<tr data-identificatore='" + temp.getId() + "'>";
								risultato += "    <td>" + temp.getTitolo() + "</td>";
								risultato += "    <td>" + temp.getDescrizione().substring(0, 60) + "...</td>";
								risultato += "    <td><button type='button' class='btn btn-danger btn-block' onclick='eliminaArticolo(this)'>E</button></td>";
								risultato += "    <td><button type='button' class='btn btn-warning btn-block' onclick='modificaArticolo(this)'>M</button></td>";
								risultato += "</tr>";
							}
							
							out.print(risultato);
						%>
					
					</tbody>
				</table>
				
			</div>
		</div>
		<!-- 
		<div class="row">
			 <div class="col-md-6">
				<button type="button" class="btn btn-info btn-block" id="cta-console">Scrivi in console</button>
			</div>
			<div class="col-md-6">
				<button type="button" class="btn btn-primary btn-block" id="cta-leggi">Leggi i libri</button>
			</div>
		</div> -->
	</div>
	
	<!--  Modale inserimento: START  -->
	<div class="modal fade" id="modaleInserimento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Inserisci Articolo</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      		<p>Riempi i campi sottostanti per inserire un nuovo articolo</p>
				<div class="form-group">
					<label for="input_titolo">Titolo:</label>
					<input type="text" class="form-control" id="titolo" />
				</div>
				
				<div class="form-group">
					<label for="input_descrizione">Descrizione:</label>
					<textarea type="text" class="form-control" id="descrizione"></textarea>
				</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="cta-inserisci">Inserisci</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!--  Modale inserimento: END  -->
	
	<!--  Modale modifica: START  -->
	<div class="modal fade" id="modaleModifica" data-identificatore="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Aggiorna Articolo</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      		<p>Riempi i campi sottostanti per modificare un articolo esistente</p>
				<div class="form-group">
					<label for="input_titolo">Titolo:</label>
					<input type="text" class="form-control" id="titolo_edit" />
				</div>
				
				<div class="form-group">
					<label for="input_descrizione">Descrizione:</label>
					<textarea type="text" class="form-control" id="descrizione_edit"></textarea>
				</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-success" id="cta-modifica">Modifica</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!--  Modale modifica: END  -->
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  	
  </body>
</html>